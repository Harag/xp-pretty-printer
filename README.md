# xp-pretty-printer

This is the November, 26 1991 version of
Richard C. Waters' XP pretty printer.

;------------------------------------------------------------------------

;Copyright Massachusetts Institute of Technology, Cambridge, Massachusetts.

;Permission to use, copy, modify, and distribute this software and its
;documentation for any purpose and without fee is hereby granted,
;provided that this copyright and permission notice appear in all
;copies and supporting documentation, and that the name of M.I.T. not
;be used in advertising or publicity pertaining to distribution of the
;software without specific, written prior permission. M.I.T. makes no
;representations about the suitability of this software for any
;purpose.  It is provided "as is" without express or implied warranty.

;    M.I.T. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
;    ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
;    M.I.T. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
;    ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
;    WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
;    ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
;    SOFTWARE.

;------------------------------------------------------------------------


It was the only copy of the code I could find, if anybody has a newer version please let me know.

A technical report describing it can be found here https://dspace.mit.edu/bitstream/handle/1721.1/6503/AIM-1102.pdf 

This library was an intrinsic part of this discussion http://www.lispworks.com/documentation/HyperSpec/Issues/iss270_w.htm

These by the same author might also interest you:

Some Useful Lisp Algorithms: Part 1
https://www.merl.com/publications/docs/TR91-04.pdf

Some Useful Lisp Algorithms: Part 2
https://www.merl.com/publications/docs/TR93-17.pdf